from c2_x import Node

"""
First approach record what has been seen, if we come across duplicate,
remove it (use hash for O(1) lookup)
Update: using set instead of dict is slightly better, no unnecassar val storage

For removal without buffer, I'll have to comapre each val to every other val
O(n^2)
"""
def removeDup(n): # n is of type Node
    prevNode = None
    valsSeen = {}
    while n is not None:
        if n.val not in valsSeen:
            valsSeen[n.val] = 1
            # Update prevNode only if we didn't remove last node and hence
            # pevNode changed
            prevNode = n
        else:
            # remove
            if prevNode is not None:
                prevNode.next = n.next

        n = n.next

"""
Update after reading book solution:
prevTestNode below can be avoided completely by starting testNode at n,
and testing testNode.next each time. (TODO: might want to add a third
solution which does that)

"""
def removeDupNoBuff(n):
    while n is not None:
        testNode = n.next
        prevTestNode = n
        while testNode is not None:
            if testNode.val == n.val:
                prevTestNode.next = testNode.next # Remove testNode
            else:
                # update prevTestNode
                prevTestNode = testNode

            # prevTestNode not updated if we deleted last element and thus the prev
            # one is same as before
            testNode = testNode.next
        n = n.next



node8 = Node(5)
node7 = Node(5, node8)
node6 = Node(2, node7)
node5 = Node(3, node6)
node4 = Node(1, node5)
node3 = Node(2, node4)
node2 = Node(1, node3)
node1 = Node(1, node2)

print(node1.printList())
removeDupNoBuff(node1)
print(node1.printList())