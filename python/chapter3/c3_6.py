"""
For dequeueAny I return the oldest cat or dog depending on which type there are
more of. The book solution returns the oldest regardless of type which can
be done through a simple global counter for the number of animals. (The
question seems to require this, though I don't think it's very clear regarding
the dequeueAny behaviour, hence my simplification)

"""
class Cat:
    def __init__(self):
        self.Name = "Cat"
class Dog:
    def __init__(self):
        self.Name = "Dog"


class Queue:
    def __init__(self):
        self.queue = []

    def enqueue(self, val):
        self.queue.append(val)
    def dequeue(self):
        return self.queue.pop(0)

class Solution:
    def __init__(self):
        self.catQueue = Queue()
        self.dogQueue = Queue()

    def enqueue(self, animal):
        if type(animal) == Cat:
            self.catQueue.enqueue(animal)
        elif type(animal) == Dog:
            self.dogQueue.enqueue(animal)
        else:
            throw Exception("In valid animal type")
    def dequeueAny(self)
        if len(self.catQueue) < len(self.dogQueue):
            return self.catQueue.dequeue()
        else:
            return self.dogQueue.dequeue()

    def dequeueCat(self):
        return self.catQueue.dequeue()
    def dequeueCat(self):
        return self.dogQueue.dequeue()