"""
Each stack element could have a new attribute, minThen, that refers to the min
as of that element, so if elements are popped and the min is popped we still
have O(1) access to the min available at the time.

Book Solution: Another solution is proposed to reduce the space usage by
keeping track of the mins in a separate stack. If a value is popped and it's
the current min, the min will also be removed the min stacks. (This
is potentially more space efficient. Consider adding a sorted range of numbers
in decreasing order, this approach will still use the same amount of space,
thus the asymptotic behaviour hasn't changed)

"""