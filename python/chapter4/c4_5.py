"""
Approach 1: For a BTS: for every node all the values on the left sub tree
must be smaller to the right sub tree. (The node may have a value equal to a
value in its left subtree) [The definition regarding repeated values can
differ]

This gives rise to a recursive solution where we check each subtree is a BTS
and check that the children along with the root are still a BTS

When combining 2 subtrees and checking if combination is still a BTS, we
need to know the overall maximum value in the left subtree and overall minimum
value in the right subtree and check that the parent on these two subtrees is
>= left subtree max, and < right subtree min

Update after reading book solution: The book also suggests an in-order
traversal approach (with an optimisation) however that can't handle duplicates

The second approach is similar to what I've implemented, although my approach
is bottom up where as the book solution is top down. The complexity of my
implementation is still the same as the book, O(N) time complexity where N
is the number of nodes in the tree. The space complexity is O(log N) on a
balanced tree, but up to O(N) for unbalanced trees, due to the recursive calls

"""

class Node:
    def __init__(self, val, leftChild=None, rightChild=None):
        self.value = val
        self.leftChild = leftChild
        self.rightChild = rightChild
        self.visited = False

def is_BTS_helper(tree):
    if tree is None:
        return (True, float("-inf"), float("inf"))

    left_is_BTS, left_max, left_min = is_BTS_helper(tree.leftChild)
    right_is_BTS, right_max, right_min = is_BTS_helper(tree.rightChild)

    if (left_is_BTS and right_is_BTS
        and tree.value >= left_max
        and tree.value < right_min):
        max = tree.value if tree.value > right_max else right_max
        min = tree.value if tree.value < left_min else left_min
        # print(f"Called on {tree.value}, returning (True, {max},{min})")
        return (True, max, min)
    return (False, None, None) # The 2nd and third parameter will never be read
def is_BTS(tree):
    if tree is None:
        return True
    return is_BTS_helper(tree)[0]

"""
         4
       /   \
     3       10
    / \      /
   1   4    8
             \
              9
"""
n1 = Node(1)
n4_1 = Node(4)
n3 = Node(3, n1, n4_1)
n9 = Node(9)
n8 = Node(8, None, n9)
n10 = Node(10, n8)
n4 = Node(4, n3, n10)
print(is_BTS(n4)) # True
n3.value = 10
print(is_BTS(n3)) # False
print(is_BTS(n4)) # False
