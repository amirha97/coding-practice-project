class Node:
    def __init__(self, val, neighbours=None):
        self.value = val
        self.neighbours = [] if neighbours is None else neighbours
        self.visited = False
class Graph:
    def __init__(self, nodes):
        self.nodes = nodes

    def resetVisited(self):
        for n in self.nodes:
            n.visited = False

def isRouteBetween(g, a, b):
    if a.value == b.value:
        return True
    g.resetVisited()
    # Given Graph g, nodes a and b
    # Breadth First search
    # assumes the graph is connected
    d = [a] # Start searching from a
    a.visited = True
    # visited is set to True when nodes are added to the queue (implemented using list)
    while len(d) > 0:
        node = d.pop(0)
        for n in node.neighbours:
            if (n.value == b.value):
                # Found b, when searching from a, hence route exists
                return True
            if not n.visited:
                n.visited = True
                d.append(n)

    return False

"""
         4
       /   \
     3       5
    / \      /
   1   2    9

"""
n1 = Node(1)
n2 = Node(2)
n3 = Node(3, [n1, n2])
n9 = Node(9)
n5 = Node(5, [n9])
n4 = Node(4, [n3, n5])
nodes = [n1, n2, n3, n9, n5, n4]
g  = Graph(nodes)
print(isRouteBetween(g, n4, n9)) # Should be True
print(isRouteBetween(g, n1, n9)) # Should be False
print(isRouteBetween(g, n3, n5)) # Should be False
print(isRouteBetween(g, n4, n1)) # Should be True
print(isRouteBetween(g, n1, n4)) # Should be False
