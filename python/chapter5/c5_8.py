"""
Approach 1: Height is just (len(screen) * 8)/width
Convert x1 which is a pixel position to a position, p1, within a byte, b1, by
b1 = x1 // 8
p1 = x1 % 8
Create a mask of form 001111, where the 1s start from the position of x1 and OR
it with the byte b.

Do a similar calculation for x2, but the mask will be of form 1111000
If b2 - b1 > 1, set every bye in between them to 0xFF (all 1s)

"""
