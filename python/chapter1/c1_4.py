"""
First approach: generate all permutation and check if pallindrome (too slow O(N!))

Second: By looking at attributes of palindromes we observe that if the number 
of characters is even (excluding spaces) then there should be an even number of 
each character. If the total character number is odd, then only one character can
have an odd repetition

(A) As in the book solution, there can't be a character with an odd repetition, in an
even length character. This means we can just check that there's at most 1 char
with odd repetition.
"""

def isPermPallindrome(a):
    a = a.replace(" ", "") # Remove all spaces
    d = {}
    for char in a:
        d[char] = d.get(char, 0) + 1
        
    # allowOdd = True if (len(a)%2 == 1) else False # As per (A) this check is not needed
    allowOdd = True # Just ensure there's at most 1 char with odd repetition
    for char in d:
        if (d[char] % 2 == 1): 
            if allowOdd:
                allowOdd = False # Only allow one 1 odd character
            else:
                return False
        else:
            continue
    
    return True

print(isPermPallindrome("taco cat"))
print(isPermPallindrome("taco   cat"))
print(isPermPallindrome("co a  t  cat"))
print(isPermPallindrome("abcde"))