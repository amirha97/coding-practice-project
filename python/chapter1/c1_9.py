"""
Approach 1: Go through the chars in the second string until it matches
the first and then continue the match until then end (if possible, otherwise
try to find new match) Then call isSubstring on the second string from
the start until where the match was found


"""
def isRotation(a, b):
    index = 0
    matchIndex = len(b) # Initially no match
    inMatch = False
    for char in b:
        if a[index] == char:
            if not inMatch:
                matchIndex = index
                inMatch = True

        else:
            inMatch = False
            matchIndex = len(b) # I could instead check of the inMatch flag is
            # set before calling isSubstring at the end

        if index == len(b) - 1:
            return isSubstring(a, b[:matchIndex])

        index += 1


# Above is broken and I ran out of time :(

def isSubstring(a, b):
    return b in a

"""
Book solution:
If lengths of `a` and `b` are the same, and `b` is a substring of `a+a`
then `b` must be a rotation of `a`

Note: The solution in the book also checks that they are non-empty
I don't think that's correct since I think an empty string is a rotation
of an empty string (requires clarification in interview)
"""

def isRotation2(a, b):
    if len(a) == len(b):
        return isSubstring(a+a, b)
    else:
        return False

print(isRotation2("abc", "cab"))
print(isRotation2("waterbottle", "erbottlewat"))
print(isRotation2("12345", "34512"))
print(isRotation2("123456", "34512"))
